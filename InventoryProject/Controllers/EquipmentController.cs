﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using InventoryProject.Helpers;
using InventoryProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace InventoryProject.Controllers
{
    public class EquipmentController : Controller
    {
        private readonly InventoryContext db;
        public EquipmentController(InventoryContext context)
        {
            db = context;
        }

        //Validation
        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyDiameter(int diameterInner, int diameterOuter)
        {
           if (diameterInner >= diameterOuter)
            {
                return Json($"Inner diameter must be smaller than outer diameter.");
            }
            return Json(true);
        }
        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyOperatingTime(int OperatingTimeMin, int OperatingTime)
        {
            if (OperatingTimeMin > OperatingTime)
            {
                return Json($"Minimal Operating Time cannot be larger than default Operating time.");
            }
            return Json(true);
        }

        // SEARCHING
        private List<Equipment> Searching(List<Equipment> equipment, string searchStatus, string searchField, string searchString)
        {
            // search field
            int result;
            bool isInt = int.TryParse(searchString, out result);
            if (string.IsNullOrEmpty(searchString)) searchString = "";
            switch (searchField)
            {
                case "Department":
                    equipment = equipment.Where(x => x.Department.ToLower()
                        .Contains(searchString.ToLower())).ToList();
                    break;
                case "Category":
                    equipment = equipment.Where(x => x.Category.ToLower()
                        .Contains(searchString.ToLower())).ToList();
                    break;
                case "Type":
                    equipment = equipment.Where(x => x.Type.ToLower()
                        .Contains(searchString.ToLower())).ToList();
                    break;
                case "Title":
                    equipment = equipment.Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower())).ToList();
                    break;
                case "SerialNumber":
                    equipment = equipment.Where(x => x.SerialNum.ToLower()
                        .Contains(searchString.ToLower())).ToList();
                    break;
                case "DiameterOuter":
                    if (isInt)
                        equipment = equipment.Where(x => x.DiameterOuter == result).ToList();
                    else
                        equipment = equipment.Where(x => x.Id == null).ToList();
                    break;
                case "DiameterInner":
                    if (isInt)
                        equipment = equipment.Where(x => x.DiameterInner == result).ToList();
                    else
                        equipment = equipment.Where(x => x.Id == null).ToList();
                    break;
                case "Length":
                    if (isInt)
                        equipment = equipment.Where(x => x.Length == result).ToList();
                    else
                        equipment = equipment.Where(x => x.Id == null).ToList();
                    break;
                default:
                    break;
            }
            // search status
            int status = -1;
            if (!string.IsNullOrEmpty(searchStatus))
            {
                if (!int.TryParse(searchStatus, out status))
                {
                    status = -1;
                }
            }
            if (status != -1)
            {
                equipment = equipment.Where(x => x.Status.Id == status).ToList();
            }

            return equipment;
        }
        // SORTING
        private List<Equipment> Sorting(string sortOrder, List<Equipment> equipment)
        {
            switch (sortOrder)
            {
                case "length_desc":
                    equipment = equipment.OrderByDescending(x => x.Length).ToList();
                    break;
                case "length":
                    equipment = equipment.OrderBy(x => x.Length).ToList();
                    break;
                case "diameterinner_desc":
                    equipment = equipment.OrderByDescending(x => x.DiameterInner).ToList();
                    break;
                case "diameterinner":
                    equipment = equipment.OrderBy(x => x.DiameterInner).ToList();
                    break;
                case "diameterouter_desc":
                    equipment = equipment.OrderByDescending(x => x.DiameterOuter).ToList();
                    break;
                case "diameterouter":
                    equipment = equipment.OrderBy(x => x.DiameterOuter).ToList();
                    break;
                case "serialnum_desc":
                    equipment = equipment.OrderByDescending(x => x.SerialNum).ToList();
                    break;
                case "serialnum":
                    equipment = equipment.OrderBy(x => x.SerialNum).ToList();
                    break;
                case "title_desc":
                    equipment = equipment.OrderByDescending(x => x.Title).ToList();
                    break;
                case "title":
                    equipment = equipment.OrderBy(x => x.Title).ToList();
                    break;
                case "type_desc":
                    equipment = equipment.OrderByDescending(x => x.Type).ToList();
                    break;
                case "type":
                    equipment = equipment.OrderBy(x => x.Type).ToList();
                    break;
                case "category_desc":
                    equipment = equipment.OrderByDescending(x => x.Category).ToList();
                    break;
                case "category":
                    equipment = equipment.OrderBy(x => x.Category).ToList();
                    break;
                case "department_desc":
                    equipment = equipment.OrderByDescending(x => x.Department).ToList();
                    break;
                case "department":
                    equipment = equipment.OrderBy(x => x.Department).ToList().ToList();
                    break;
                case "status_desc":
                    equipment = equipment.OrderByDescending(x => x.Status.Title).ToList();
                    break;
                default:
                    equipment = equipment.OrderBy(x => x.Status.Title).ToList();
                    break;
            }
            return equipment;
        }

        // Get orders
        private List<Order> GetOrders(List<Equipment> equipment)
        {
            List<Order> orders = new List<Order>();
            foreach (var item in equipment)
            {
                if (item.Status.Title == "JF")
                {
                    var tempOrders = db.Orders
                        .Include(x => x.Equipment)
                        .Include(x => x.Contract)
                        .Include(x => x.Well)
                        .ThenInclude(x => x.Client)
                        .Where(x => x.Equipment.Id == item.Id && x.IsOpen == true).ToList();
                    orders.AddRange(tempOrders);
                }
            }
            return orders;
        }

        // GET: EquipmentController
        public ActionResult Index(string oldSortOrder, string newSortOrder, string searchString, string searchField, string searchStatus, int? page)
        {
            ViewBag.CurrentFilter = searchString;
            ViewBag.SearchStatus = string.IsNullOrEmpty(searchStatus) ? "-1" : searchStatus;
            ViewBag.SearchField = string.IsNullOrEmpty(searchField) ? "Department" : searchField; 
            ViewBag.StatusList = db.EquipmentStatuses.ToList();
            var equipment = db.Equipment.Include(x => x.Status).ToList();
            ViewBag.Orders = GetOrders(equipment);

            // SEARCH
            equipment = Searching(equipment, searchStatus, searchField, searchString);

            // SORTING
            if (string.IsNullOrEmpty(newSortOrder))
            {
                newSortOrder = "status";
            }
            else if (newSortOrder == oldSortOrder)
            {
                newSortOrder += "_desc";
            }          
            ViewBag.CurrentSort = newSortOrder;
            equipment = Sorting(newSortOrder, equipment);

            if (searchString != null)
            {
                page = 1;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);


            return View(equipment.ToPagedList(pageNumber, pageSize));
        }

        // GET: EquipmentController/Details/5
        public ActionResult Details(int id)
        {
            var equipment = db.Equipment.Include("Status").FirstOrDefault(x => x.Id == id);
            switch (equipment.Status.Title)
            {
                case "RFU":
                    ViewBag.ClientList = db.Clients.OrderBy(x => x.Name).ToList();
                    ViewBag.ContractList = db.Agreements.OrderBy(x => x.Client.Id).ToList();
                    ViewBag.WellList = db.Wells.OrderBy(x => x.Client.Id).ToList();
                    break;
                case "JF":
                    var order = db.Orders.Include(x => x.Contract)
                            .ThenInclude(contract => contract.Client)
                        .Include(x => x.Well)
                        .Include(x => x.Equipment)
                        .FirstOrDefault(x => x.Equipment.Id == equipment.Id
                            && x.IsOpen == true);
                    ViewBag.Order = order;
                    break;
                case "WS":
                    break;
                case "SP":
                case "RP":
                    var maintenance = db.Maintenances.Include(x => x.Equipment)
                            .ThenInclude(equip => equip.Status)
                            .FirstOrDefault(x => x.Equipment.Id == equipment.Id
                            && x.IsOpen == true);
                    ViewBag.Maintenance = maintenance;
                    ViewBag.Scans = db.DocScans.Where(x => x.Maintenance.Id == maintenance.Id).ToList();
                    break;
                default:
                    break;
            }
            ViewBag.History = db.History.Include(x => x.Type)
                .Where(x => x.Equipment == equipment).ToList();
            return View(equipment);
        }

        // CREATE
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Equipment equipment)
        {
            if (ModelState.IsValid)
            {
                var status = db.EquipmentStatuses.FirstOrDefault(x => x.Title == "RFU");
                equipment.Status = status;
                equipment.OperatingTime *= 60; // in minutes
                equipment.OperatingTimeMin *= 60; // in minutes
                db.Equipment.Add(equipment);

                var type = db.HistoryTypes.FirstOrDefault(x => x.Type == "General");
                History history = new History()
                {
                    Date = DateTime.Now,
                    EquipmentStatus = status.Title,
                    Type = type,
                    Equipment = equipment,
                    Commentary = "Added to Database / Arrived to storage"
                };
                db.History.Add(history);

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // EDIT INFO
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEquipment(int equipmentId, 
                string editDepartment, string editCategory, 
                string editType, string editTitle, 
                string editSerialNum, string editDiameterOuter, 
                string editDiameterInner, string editLength,
                string editOperatingTime, string editOperatingTimeMin)
        {
            try
            {
                var equipment = db.Equipment.Include(x => x.Status)
                    .FirstOrDefault(x => x.Id == equipmentId);
                equipment.Department = editDepartment;
                equipment.Category = editCategory;
                equipment.Type = editType;
                equipment.Title = editTitle;
                equipment.SerialNum = editSerialNum;
                equipment.DiameterOuter = int.Parse(editDiameterOuter);
                equipment.DiameterInner = int.Parse(editDiameterInner);
                equipment.Length = int.Parse(editLength);
                equipment.OperatingTime = int.Parse(editOperatingTime) * 60;
                equipment.OperatingTimeMin = int.Parse(editOperatingTimeMin) * 60;

                var type = db.HistoryTypes.FirstOrDefault(x => x.Type == "General");
                History history = new History()
                {
                    Date = DateTime.Now,
                    EquipmentStatus = equipment.Status.Title,
                    Type = type,
                    Equipment = equipment,
                    Commentary = "Properties edited"
                };
                db.History.Add(history);

                db.SaveChanges();
                return RedirectToAction("Details", new { id = equipmentId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = equipmentId });
            }
        }


        // OPEN WORK ORDER
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> OpenOrder(int equipmentId, string orderContract, string orderWell)
        {
            try
            {
                var equipment = db.Equipment.Include(x => x.Status)
                    .FirstOrDefault(x => x.Id == equipmentId);
                equipment.Status = db.EquipmentStatuses.FirstOrDefault(x => x.Title == "JF");
                
                var contract = db.Agreements.FirstOrDefault(x => x.Id == int.Parse(orderContract));
                var well = db.Wells.FirstOrDefault(x => x.Id == int.Parse(orderWell));
                
                int? newOrderId = db.Orders.Max(x => (int?)x.Id);
                if (newOrderId == null)
                {
                    newOrderId = 0;
                }
                Order order = new Order() { 
                    Id = (int)newOrderId + 1,
                    Equipment = equipment, 
                    Contract = contract, 
                    Well = well, 
                    IsOpen = true, 
                    OrderStart = DateTime.Now 
                };
                db.Orders.Add(order);

                var type = db.HistoryTypes.FirstOrDefault(x => x.Type == "Order");
                History history = new History()
                {
                    Date = DateTime.Now,
                    EquipmentStatus = equipment.Status.Title,
                    Type = type,
                    EntityID = (int)newOrderId + 1,
                    Equipment = equipment,
                    Commentary = "Order started"
                };
                db.History.Add(history);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = equipmentId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = equipmentId });
            }            
        }

        // START MAINTENANCE
        public async Task<ActionResult> StartMaintenance(int id)
        {
            try
            {
                var equipment = db.Equipment.Include(x => x.Status)
                    .FirstOrDefault(x => x.Id == id);
                equipment.Status = db.EquipmentStatuses.FirstOrDefault(x => x.Title == "SP");

                int? newMaintenanceId = db.Orders.Max(x => (int?)x.Id);
                if (newMaintenanceId == null)
                {
                    newMaintenanceId = 0;
                }
                Maintenance maintenance = new Maintenance() { 
                    Id = (int)newMaintenanceId + 1,
                    Equipment = equipment,
                    MaintenanceStart = DateTime.Now,
                    Title = "WO-"+ ((int)newMaintenanceId + 1) +"/" + DateTime.Now.Year,
                    IsOpen = true 
                };
                db.Maintenances.Add(maintenance);

                var type = db.HistoryTypes.FirstOrDefault(x => x.Type == "Maintenance");
                History history = new History()
                {
                    Date = DateTime.Now,
                    EquipmentStatus = equipment.Status.Title,
                    Type = type,
                    EntityID = (int)newMaintenanceId + 1,
                    Equipment = equipment,
                    Commentary = "Maintenance started"
                };
                db.History.Add(history);

                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id });
            }
            catch
            {
                return RedirectToAction("Details", new { id });
            }
        }

        // GET: EquipmentController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: EquipmentController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
