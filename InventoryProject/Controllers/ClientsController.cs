﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using InventoryProject.Controllers.Interfaces;
using InventoryProject.Helpers;
using InventoryProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace InventoryProject.Controllers
{
    public class ClientsController : Controller
    {
        private readonly InventoryContext db;
        public ClientsController(InventoryContext context)
        {
            db = context;
        }

        // SORTING
        private List<Client> Sorting(string sortOrder, List<Client> clients)
        {
            switch (sortOrder)
            {
                case "id_desc":
                    clients = clients.OrderByDescending(s => s.Id).ToList();
                    break;
                case "id":
                    clients = clients.OrderBy(s => s.Id).ToList();
                    break;
                case "name_desc":
                    clients = clients.OrderByDescending(s => s.Name).ToList();
                    break;
                default:
                    clients = clients.OrderBy(s => s.Name).ToList();
                    break;
            }
            return clients;
        }

        // LIST
        public ActionResult Index(string oldSortOrder, string newSortOrder, string searchString, int? page, string currentFilter)
        {
            var clients = db.Clients.ToList();

            if (!string.IsNullOrEmpty(searchString))
            {
                clients = clients.Where(s => s.Name.ToLower().Contains(searchString.ToLower())).ToList(); 
            }

            if (string.IsNullOrEmpty(newSortOrder))
            {
                newSortOrder = "name";
            }
            else if (newSortOrder == oldSortOrder)
            {
                newSortOrder += "_desc";
            }
            ViewBag.CurrentSort = newSortOrder;
            clients = Sorting(newSortOrder, clients);

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.ClientContracts = db.Agreements.Include(x => x.Client).ToList();
            ViewBag.ClientWells = db.Wells.Include(x => x.Client).ToList();
            return View(clients.ToPagedList(pageNumber, pageSize));
        }

        // DETAILS 
        [HttpGet("/[controller]/[action]/{id}")]
        public ActionResult Details(int id)
        {
            var client = db.Clients.FirstOrDefault(x => x.Id == id);
            ViewBag.ClientContracts = db.Agreements.Where(x => x.Client.Id == client.Id).ToList();
            ViewBag.ClientWells = db.Wells.Where(x => x.Client.Id == client.Id).ToList();
            return View(client);
        }

        // CREATE CLIENT
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Client client)
        {
            if (ModelState.IsValid)
            {
                db.Clients.Add(client);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // ADD CONTRACT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddContract(int clientId, string contractNum)
        {
            try
            {
                var client = db.Clients.FirstOrDefault(x => x.Id == clientId);
                db.Agreements.Add(new Contract() { ContractNumber = contractNum, Client = client });
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = clientId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = clientId });
            }
        }

        // ADD WELL
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddWell(int clientId, string title)
        {
            try
            {
                var client = db.Clients.FirstOrDefault(x => x.Id == clientId);
                db.Wells.Add(new Well() { Title = title, Client = client });
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = clientId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = clientId });
            }
        }

        // EDIT CONTRACT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditContract(int clientId, int contractId, string editContractNum)
        {
            try
            {
                var contract = db.Agreements.FirstOrDefault(x => x.Id == contractId);
                contract.ContractNumber = editContractNum;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = clientId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = clientId });
            }
        }

        // EDIT WELL
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditWell(int clientId, int wellId, string editTitle)
        {
            try
            {
                var well = db.Wells.FirstOrDefault(x => x.Id == wellId);
                well.Title = editTitle;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = clientId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = clientId });
            }
        }

        // EDIT CLIENT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditClient(int clientId, string editName)
        {
            try
            {
                var client = db.Clients.FirstOrDefault(x => x.Id == clientId);
                client.Name = editName;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = clientId });
            }
            catch
            {
                return RedirectToAction("Details", new { id = clientId });
            }
        }
    }




    // TODO : Removing Clients, Contracts, Wells
    // GET: ClientsController/Delete/5
    //public ActionResult Delete(int id)
    //{
    //    return View();
    //}

    //// POST: ClientsController/Delete/5
    //[HttpPost]
    //[ValidateAntiForgeryToken]
    //public ActionResult Delete(int id, IFormCollection collection)
    //{
    //    try
    //    {
    //        return RedirectToAction(nameof(Index));
    //    }
    //    catch
    //    {
    //        return View();
    //    }
    //}
}
