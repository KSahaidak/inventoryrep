﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Controllers.Interfaces
{
    interface IExcelDataExport
    {
        public IActionResult ExportToExcel();
    }
}
