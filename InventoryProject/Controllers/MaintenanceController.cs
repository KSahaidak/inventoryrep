﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace InventoryProject.Controllers
{
    public class MaintenanceController : Controller
    {
        private readonly InventoryContext db;
        public MaintenanceController(InventoryContext context)
        {
            db = context;
        }
        // SORTING
        private List<Maintenance> Sorting(List<Maintenance> maintenance, string sortOrder)
        {
            switch (sortOrder)
            {
                case "status_desc":
                    maintenance = maintenance.OrderBy(x => x.IsOpen).ToList();
                    break;
                case "status":
                    maintenance = maintenance.OrderByDescending(x => x.IsOpen).ToList();
                    break;
                case "datestart_desc":
                    maintenance = maintenance.OrderByDescending(x => x.MaintenanceStart).ToList();
                    break;
                case "datestart":
                    maintenance = maintenance.OrderBy(x => x.MaintenanceStart).ToList();
                    break;
                case "datefinish_desc":
                    maintenance = maintenance.OrderByDescending(x => x.MaintenanceFinish).ToList();
                    break;
                case "datefinish":
                    maintenance = maintenance.OrderBy(x => x.MaintenanceFinish).ToList();
                    break;
                case "id_desc":
                    maintenance = maintenance.OrderByDescending(x => x.Id).ToList();
                    break;
                default:
                    maintenance = maintenance.OrderBy(x => x.Id).ToList();
                    break;
            }
            return maintenance;
        }
        // LIST
        public ActionResult Index(string oldSortOrder, string newSortOrder, int? page)
        {
            var maintenance = db.Maintenances.Include(x => x.Equipment).ToList();
            
            // SORTING           
            if (string.IsNullOrEmpty(newSortOrder))
            {
                newSortOrder = "id";
            }
            else if (newSortOrder == oldSortOrder)
            {
                newSortOrder += "_desc";
            }
            ViewBag.CurrentSort = newSortOrder;
            maintenance = Sorting(maintenance, newSortOrder);

            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(maintenance.ToPagedList(pageNumber, pageSize));
        }

        // GET: MaintenanceController/Details/5
        public ActionResult Details(int id)
        {
            var maintenance = db.Maintenances
                .Include(x => x.Equipment)
                .ThenInclude(x => x.Status)
                .FirstOrDefault(x => x.Id == id);
            ViewBag.History = db.History.Include(x => x.Type)
                .Where(x => x.EntityID == id).ToList();
            ViewData["Scans"] = db.DocScans.Where(x => x.Maintenance.Id == maintenance.Id).ToList();
            return View(maintenance);
        }
        
        //// GET: MaintenanceController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: MaintenanceController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
