﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace InventoryProject.Controllers
{
    public class OrdersController : Controller
    {
        private readonly InventoryContext db;
        public OrdersController(InventoryContext context)
        {
            db = context;
        }

        // SEARCH
        private List<Order> Searching(List<Order> orders, string searchStatus, string searchField, string searchString)
        {
            switch (searchField)
            {
                case "Client":
                        orders = orders.Where(x => x.Contract.Client.Name.ToLower().Contains(searchString)).ToList();
                    break;
                case "Contract":
                        orders = orders.Where(x => x.Contract.ContractNumber.ToLower().Contains(searchString)).ToList();
                    break;
                case "Well":
                        orders = orders.Where(x => x.Well.Title.ToLower().Contains(searchString)).ToList();
                    break;
                default:
                    break;
            }
            if (searchStatus == "Open")
            {
                orders = orders.Where(x => x.IsOpen == true).ToList();
            }
            else
            {
                orders = orders.Where(x => x.IsOpen == false).ToList();
            }
            return orders;
        }
        // SORT
        private List<Order> Sorting(List<Order> orders, string sortOrder)
        {
            switch (sortOrder)
            {
                case "well_desc":
                    orders = orders.OrderByDescending(x => x.Well.Title).ToList();
                    break;
                case "well":
                    orders = orders.OrderBy(x => x.Well.Title).ToList();
                    break;
                case "contract_desc":
                    orders = orders.OrderByDescending(x => x.Contract.ContractNumber).ToList();
                    break;
                case "contract":
                    orders = orders.OrderBy(x => x.Contract.ContractNumber).ToList();
                    break;
                case "client_desc":
                    orders = orders.OrderByDescending(x => x.Contract.Client.Name).ToList();
                    break;
                case "client":
                    orders = orders.OrderBy(x => x.Contract.Client.Name).ToList();
                    break;
                case "id_desc":
                    orders = orders.OrderByDescending(x => x.Id).ToList();
                    break;
                default:
                    orders = orders.OrderBy(x => x.Id).ToList();
                    break;
            }
            return orders;
        }

        // LIST
        public ActionResult Index(string oldSortOrder, string newSortOrder, string searchString, string searchField, string searchStatus, int? page)
        {
            ViewBag.CurrentFilter = searchString;

            ViewBag.SearchStatus = string.IsNullOrEmpty(searchStatus) ? "Open" : searchStatus;
            ViewBag.SearchField = string.IsNullOrEmpty(searchField) ? "Client" : searchField;
            
            var orders = db.Orders.Include(x => x.Contract)
                         .ThenInclude(contract => contract.Client)
                         .Include(x => x.Well)
                         .Include(x => x.Equipment).ToList();

            // SEARCH
            if (string.IsNullOrEmpty(searchStatus))
                searchStatus = "Open";
            if (string.IsNullOrEmpty(searchString))
                searchString = "";
            orders = Searching(orders, searchStatus, searchField, searchString);

            // SORTING
            if (string.IsNullOrEmpty(newSortOrder))
            {
                newSortOrder = "id"; 
            }
            else if (newSortOrder == oldSortOrder)
            {
                newSortOrder += "_desc";                 
            }
            orders = Sorting(orders, newSortOrder);
            ViewBag.CurrentSort = newSortOrder;

            if (searchString != null)
            {
                page = 1;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            return View(orders.ToPagedList(pageNumber, pageSize));
        }

        // GET: OrdersController/Details/5
        public ActionResult Details(int id)
        {
            var order = db.Orders.Include(x => x.Contract)
                            .ThenInclude(contract => contract.Client)
                        .Include(x => x.Well)
                        .Include(x => x.Equipment)
                        .FirstOrDefault(x => x.Id == id);

            ViewBag.History = db.History.Include(x => x.Type)
                .Where(x => x.EntityID == id).ToList();
            return View(order);
        }


        // GET: OrdersController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: OrdersController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
