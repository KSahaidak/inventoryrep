﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class InventoryContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Contract> Agreements { get; set; }
        public DbSet<Well> Wells { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<EquipmentStatus> EquipmentStatuses { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Maintenance> Maintenances { get; set; }
        public DbSet<History> History { get; set; }
        public DbSet<HistoryType> HistoryTypes { get; set; }
        public DbSet<DocScan> DocScans { get; set; }
        public InventoryContext(DbContextOptions<InventoryContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=IL707; Database=InventoryDB; Trusted_Connection=True;");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            EquipmentStatus[] statuses = {
                new EquipmentStatus() { Id = 1, Title = "RFU" },
                new EquipmentStatus() { Id = 2, Title = "JF" },
                new EquipmentStatus() { Id = 3, Title = "WS" },
                new EquipmentStatus() { Id = 4, Title = "SP" },
                new EquipmentStatus() { Id = 5, Title = "RP" },
                new EquipmentStatus() { Id = 6, Title = "Scrap" }};
            
            modelBuilder.Entity<EquipmentStatus>().HasData(statuses);
            
            HistoryType[] types = {
                new HistoryType() { Id = 1, Type = "Order" },
                new HistoryType() { Id = 2, Type = "Maintenance" },
                new HistoryType() { Id = 3, Type = "General" }};
            modelBuilder.Entity<HistoryType>().HasData(types);

            modelBuilder.Entity<Order>().Property(x => x.Id).ValueGeneratedNever();
            modelBuilder.Entity<Maintenance>().Property(x => x.Id).ValueGeneratedNever();
        }
    }
}
