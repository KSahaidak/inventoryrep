﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class HistoryType
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
