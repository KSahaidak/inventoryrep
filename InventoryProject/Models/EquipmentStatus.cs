﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class EquipmentStatus
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        [Required]
        public string Title { get; set; }
    }
}
