﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class Client
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Name")]
        [Required]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Name must contain 3 - 256 symbols")]
        public string Name { get; set; }
    }
}
