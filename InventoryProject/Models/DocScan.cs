﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class DocScan
    {
        public int Id { get; set; }
        public string base64Image { get; set; }
        public string Filename { get; set; }
        public Maintenance Maintenance { get; set; }
    }
}
