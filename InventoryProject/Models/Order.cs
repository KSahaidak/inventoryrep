﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class Order
    {
        public int Id { get; set; }
        public Equipment Equipment { get; set; }
        public Contract Contract { get; set; }
        public Well Well { get; set; }
        public bool IsOpen { get; set; }
        public DateTime OrderStart { get; set; }
        public DateTime OrderFinish { get; set; }
    }
}
