﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class Contract
    {
        public int Id { get; set; }
        public Client Client { get; set; }

        [Display(Name = "Contract Number")]
        [Required]
        public string ContractNumber { get; set; }

    }
}
