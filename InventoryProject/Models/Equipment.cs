﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class Equipment
    {
        public int Id { get; set; }

        [Display(Name = "Department")]
        [Required]
        public string Department { get; set; }

        [Display(Name = "Category")]
        [Required]
        public string Category { get; set; }

        [Display(Name = "Type")]
        [Required]
        public string Type { get; set; }

       
        [Display(Name = "Title")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Serial Number")]
        [Required]
        public string SerialNum { get; set; }

        [Display(Name = "Outer Diameter, mm")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Outer Diameter must be bigger than 0.")]
        [Remote(action: "VerifyDiameter", controller: "Equipment", AdditionalFields = nameof(DiameterInner))]
        public int DiameterOuter { get; set; }

        [Display(Name = "Inner Diameter, mm")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Inner Diameter must be bigger than 0.")]
        [Remote(action: "VerifyDiameter", controller: "Equipment", AdditionalFields = nameof(DiameterOuter))]
        public int DiameterInner { get; set; }

        [Display(Name = "Length, mm")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Length must be bigger than 0.")]
        public int Length { get; set; }

        [Display(Name = "Status")]
        public EquipmentStatus Status { get; set; }

        [Display(Name = "Operating Time (hours)")]
        [Range(0, int.MaxValue, ErrorMessage = "Operating Time must be longer than 0.")]
        [Required]
        public int OperatingTime { get; set; }

        [Display(Name = "Alert for Minimal Operating Time (hours)")]
        [Range(0, int.MaxValue, ErrorMessage = "Operating Time must be longer than 0.")]
        [Remote(action: "VerifyOperatingTime", controller: "Equipment", AdditionalFields = nameof(OperatingTime))]
        [Required]
        public int OperatingTimeMin { get; set; }

    }
}
