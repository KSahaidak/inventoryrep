﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class History
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Equipment Equipment { get; set; }
        public string EquipmentStatus { get; set; }
        public HistoryType Type { get; set; }
        public int? EntityID { get; set; }
        public string Commentary { get; set; }
        public int? OperatingHours { get; set; }
    }
}
