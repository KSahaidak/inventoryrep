﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Models
{
    public class Maintenance
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public Equipment Equipment { get; set; }
        public bool IsOpen { get; set; }
        public DateTime MaintenanceStart { get; set; }
        public DateTime MaintenanceExpectedFinish { get; set; }
        public DateTime MaintenanceFinish { get; set; }
    }
}
