﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InventoryProject.Migrations
{
    public partial class WED220703 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "Clients",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(maxLength: 256, nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Clients", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "EquipmentStatuses",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Title = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_EquipmentStatuses", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "HistoryTypes",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Type = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_HistoryTypes", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Agreements",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        ClientId = table.Column<int>(nullable: true),
            //        ContractNumber = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Agreements", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Agreements_Clients_ClientId",
            //            column: x => x.ClientId,
            //            principalTable: "Clients",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Wells",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Title = table.Column<string>(nullable: false),
            //        ClientId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Wells", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Wells_Clients_ClientId",
            //            column: x => x.ClientId,
            //            principalTable: "Clients",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Equipment",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Department = table.Column<string>(nullable: false),
            //        Category = table.Column<string>(nullable: false),
            //        Type = table.Column<string>(nullable: false),
            //        Title = table.Column<string>(nullable: false),
            //        SerialNum = table.Column<string>(nullable: false),
            //        DiameterOuter = table.Column<int>(nullable: false),
            //        DiameterInner = table.Column<int>(nullable: false),
            //        Length = table.Column<int>(nullable: false),
            //        StatusId = table.Column<int>(nullable: true),
            //        OperatingTime = table.Column<int>(nullable: false),
            //        OperatingTimeMin = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Equipment", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Equipment_EquipmentStatuses_StatusId",
            //            column: x => x.StatusId,
            //            principalTable: "EquipmentStatuses",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "History",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Date = table.Column<DateTime>(nullable: false),
            //        EquipmentId = table.Column<int>(nullable: true),
            //        EquipmentStatus = table.Column<string>(nullable: true),
            //        TypeId = table.Column<int>(nullable: true),
            //        EntityID = table.Column<int>(nullable: true),
            //        Commentary = table.Column<string>(nullable: true),
            //        OperatingHours = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_History", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_History_Equipment_EquipmentId",
            //            column: x => x.EquipmentId,
            //            principalTable: "Equipment",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_History_HistoryTypes_TypeId",
            //            column: x => x.TypeId,
            //            principalTable: "HistoryTypes",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Maintenances",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false),
            //        Title = table.Column<string>(nullable: true),
            //        EquipmentId = table.Column<int>(nullable: true),
            //        IsOpen = table.Column<bool>(nullable: false),
            //        MaintenanceStart = table.Column<DateTime>(nullable: false),
            //        MaintenanceExpectedFinish = table.Column<DateTime>(nullable: false),
            //        MaintenanceFinish = table.Column<DateTime>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Maintenances", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Maintenances_Equipment_EquipmentId",
            //            column: x => x.EquipmentId,
            //            principalTable: "Equipment",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Orders",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false),
            //        EquipmentId = table.Column<int>(nullable: true),
            //        ContractId = table.Column<int>(nullable: true),
            //        WellId = table.Column<int>(nullable: true),
            //        IsOpen = table.Column<bool>(nullable: false),
            //        OrderStart = table.Column<DateTime>(nullable: false),
            //        OrderFinish = table.Column<DateTime>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Orders", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_Orders_Agreements_ContractId",
            //            column: x => x.ContractId,
            //            principalTable: "Agreements",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Orders_Equipment_EquipmentId",
            //            column: x => x.EquipmentId,
            //            principalTable: "Equipment",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Orders_Wells_WellId",
            //            column: x => x.WellId,
            //            principalTable: "Wells",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "DocScans",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        base64Image = table.Column<string>(nullable: true),
            //        Filename = table.Column<string>(nullable: true),
            //        MaintenanceId = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_DocScans", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_DocScans_Maintenances_MaintenanceId",
            //            column: x => x.MaintenanceId,
            //            principalTable: "Maintenances",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.InsertData(
            //    table: "EquipmentStatuses",
            //    columns: new[] { "Id", "Title" },
            //    values: new object[,]
            //    {
            //        { 1, "RFU" },
            //        { 2, "JF" },
            //        { 3, "WS" },
            //        { 4, "SP" },
            //        { 5, "RP" },
            //        { 6, "Scrap" }
            //    });

            //migrationBuilder.InsertData(
            //    table: "HistoryTypes",
            //    columns: new[] { "Id", "Type" },
            //    values: new object[,]
            //    {
            //        { 1, "Order" },
            //        { 2, "Maintenance" },
            //        { 3, "General" }
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Agreements_ClientId",
            //    table: "Agreements",
            //    column: "ClientId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_DocScans_MaintenanceId",
            //    table: "DocScans",
            //    column: "MaintenanceId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Equipment_StatusId",
            //    table: "Equipment",
            //    column: "StatusId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_History_EquipmentId",
            //    table: "History",
            //    column: "EquipmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_History_TypeId",
            //    table: "History",
            //    column: "TypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Maintenances_EquipmentId",
            //    table: "Maintenances",
            //    column: "EquipmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_ContractId",
            //    table: "Orders",
            //    column: "ContractId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_EquipmentId",
            //    table: "Orders",
            //    column: "EquipmentId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_WellId",
            //    table: "Orders",
            //    column: "WellId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Wells_ClientId",
            //    table: "Wells",
            //    column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocScans");

            migrationBuilder.DropTable(
                name: "History");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Maintenances");

            migrationBuilder.DropTable(
                name: "HistoryTypes");

            migrationBuilder.DropTable(
                name: "Agreements");

            migrationBuilder.DropTable(
                name: "Wells");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "EquipmentStatuses");
        }
    }
}
