﻿using ClosedXML.Excel;
using InventoryProject.Models;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Helpers
{
    public class ExcelExportHelper
    {

        private enum ExcelExportListType { ContractList, WellList, EquipmentList };
        private int currentExcelRow = 1;
        private readonly InventoryContext db;
            
        public ExcelExportHelper(InventoryContext context)
        {
            db = context;
        }

        public IXLWorkbook Export()
        {
            var workbook = new XLWorkbook();
            IXLWorksheet worksheet = workbook.Worksheets.Add("Clients");
            worksheet.Style.Font.FontSize = 12;
            currentExcelRow = 1;
            var clients = db.Clients.ToList();
            ExcelExportTableColumnsInit(worksheet);
            foreach (var client in clients)
            {
                currentExcelRow++;
                var clientStartRow = currentExcelRow;
                worksheet.Cell(currentExcelRow, 1).Value = client.Id;
                worksheet.Cell(currentExcelRow, 2).Value = client.Name;
                worksheet.Range(currentExcelRow, 1, currentExcelRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                currentExcelRow++;
                worksheet.Range(currentExcelRow, 1, currentExcelRow, 2).Merge();
                worksheet.Range(currentExcelRow, 1, currentExcelRow + db.Agreements.Where(x => x.Client.Id == client.Id).Count(), 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(currentExcelRow, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell(currentExcelRow, 1).Value = "Контракты: ";
                ExcelExportList(worksheet, ExcelExportListType.ContractList, client.Id);
                currentExcelRow++;
                worksheet.Range(currentExcelRow, 1, currentExcelRow, 2).Merge();
                worksheet.Range(currentExcelRow, 1, currentExcelRow + db.Wells.Where(x => x.Client.Id == client.Id).Count(), 2).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                worksheet.Cell(currentExcelRow, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell(currentExcelRow, 1).Value = "Колодцы: ";
                ExcelExportList(worksheet, ExcelExportListType.WellList, client.Id);
                currentExcelRow++;
                worksheet.Range(currentExcelRow, 1, currentExcelRow, 2).Merge();
                worksheet.Cell(currentExcelRow, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                worksheet.Cell(currentExcelRow, 1).Value = "Оборудование: ";
                worksheet.Range(clientStartRow, 1, currentExcelRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
                ExcelExportList(worksheet, ExcelExportListType.EquipmentList, client.Id);
            }
            return workbook;
        }

        private void ExcelExportTableColumnsInit(IXLWorksheet worksheet)
        {
            worksheet.Range(1, 1, 1, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thick;
            worksheet.Range(1, 1, 1, 10).Style.Border.InsideBorder = XLBorderStyleValues.Thick;
            worksheet.Range(1, 1, 1, 10).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet.Range(1, 1, 1, 10).Style.Font.Bold = true;
            worksheet.Range(1, 1, 1, 10).Style.Fill.BackgroundColor = XLColor.AliceBlue;

            worksheet.Cell(currentExcelRow, 1).Value = "Номер";
            worksheet.Column(1).Width = 10;

            worksheet.Cell(currentExcelRow, 2).Value = "Название";
            worksheet.Column(2).Width = 25;

            worksheet.Cell(currentExcelRow, 3).Value = "Статус";
            worksheet.Column(3).Width = 15;

            worksheet.Cell(currentExcelRow, 4).Value = "Серийный номер";
            worksheet.Column(4).Width = 20;

            worksheet.Cell(currentExcelRow, 5).Value = "Департамент";
            worksheet.Column(5).Width = 20;

            worksheet.Cell(currentExcelRow, 6).Value = "Вид";
            worksheet.Column(6).Width = 10;

            worksheet.Cell(currentExcelRow, 7).Value = "Тип";
            worksheet.Column(7).Width = 10;

            worksheet.Cell(currentExcelRow, 8).Value = "Внутренний диаметр";
            worksheet.Column(8).Width = 25;

            worksheet.Cell(currentExcelRow, 9).Value = "Внешний диаметр";
            worksheet.Column(9).Width = 25;

            worksheet.Cell(currentExcelRow, 10).Value = "Длина";
            worksheet.Column(10).Width = 15;

        }

        private void ExcelExportList(IXLWorksheet worksheet, ExcelExportListType listType, int clientId)
        {
            var listStartRow = currentExcelRow;
            if (listType == ExcelExportListType.ContractList)
            {
                var clientContracts = db.Agreements.Where(x => x.Client.Id == clientId).ToList();
                foreach (var agreement in clientContracts)
                {
                    currentExcelRow++;
                    worksheet.Cell(currentExcelRow, 1).Value = agreement.Id;
                    worksheet.Cell(currentExcelRow, 2).Value = agreement.ContractNumber;
                }
                worksheet.Range(listStartRow, 1, currentExcelRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            }
            else if (listType == ExcelExportListType.WellList)
            {
                var clientWells = db.Wells.Where(x => x.Client.Id == clientId).ToList();
                foreach (var well in clientWells)
                {
                    currentExcelRow++;
                    worksheet.Cell(currentExcelRow, 1).Value = well.Id;
                    worksheet.Cell(currentExcelRow, 2).Value = well.Title;
                }
                worksheet.Range(listStartRow, 1, currentExcelRow, 10).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
            }
            else if (listType == ExcelExportListType.EquipmentList)
            {
                var clientOrders = db.Orders.Where(x => x.Well.Client.Id == clientId).ToList();
                List<Equipment> equipment = new List<Equipment>();
                foreach (var order in clientOrders)
                {
                    currentExcelRow++;
                    worksheet.Cell(currentExcelRow, 1).Value = order.Equipment.Id;
                    worksheet.Cell(currentExcelRow, 2).Value = order.Equipment.Title;
                    worksheet.Cell(currentExcelRow, 3).Value = order.Equipment.Status;
                    worksheet.Cell(currentExcelRow, 4).Value = order.Equipment.SerialNum;
                    worksheet.Cell(currentExcelRow, 5).Value = order.Equipment.Department;
                    worksheet.Cell(currentExcelRow, 6).Value = order.Equipment.Category;
                    worksheet.Cell(currentExcelRow, 7).Value = order.Equipment.Type;
                    worksheet.Cell(currentExcelRow, 8).Value = order.Equipment.DiameterInner;
                    worksheet.Cell(currentExcelRow, 9).Value = order.Equipment.DiameterOuter;
                    worksheet.Cell(currentExcelRow, 10).Value = order.Equipment.Length;
                }
            }
        }
    }
}
