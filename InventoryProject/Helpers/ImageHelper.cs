﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryProject.Helpers
{
    public class ImageHelper
    {
        public static string ImageToBase64(IFormFile imgfilepath)
        {
            byte[] imageBytes = ConvertToBytes(imgfilepath);
            string base64Image = Convert.ToBase64String(imageBytes);
            return base64Image;
        }

        public static byte[] ConvertToBytes(IFormFile image)
        {
            using (var memoryStream = new MemoryStream())
            {
                image.OpenReadStream().CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }
    }
}
